import os
import system

when declared(commandLineParams):
    let argv = commandLineParams()
    for i, v in argv:
        if (existsFile v) and not (existsDir v):
            try:
                var f = open(v)
                f.close()
                try:
                    f = open(v, fmWrite)
                    f.writeLine("")
                    f.close()
                except IOError:
                    echo v, ": file cannot be emptied"
                    continue
            except IOError:
                echo v, ": file cannot be read"
                continue
        elif existsDir v:
            echo v, ": is a directory"
            continue
        else:
            echo v, ": does not exist"

else:
    echo "Input a file to be emptied"
