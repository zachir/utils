#!/usr/bin/perl
use diagnostics;
use warnings;
use strict;
use Math::Trig;

$| = 1;

my $num_args = $#ARGV + 1;
if ($num_args == 0) {
	my $cmd;
	while (1) {
		print "Please enter your expression (or 'q' to quit): ";
		chomp(my $cmd = <STDIN>);
		
		exit if ($cmd =~ /^\s*q\s*$/);
		my $result = eval "$cmd";
		if (not $result) {
			$result = "\"$cmd\" does not compute"
		}
		print "Result = $result\n\n";
	}
} elsif ($num_args == 1) {
	my $cmd = $ARGV[0];
	chomp $cmd;
	my $result = eval "$cmd";
	if (not $result) {
		$result = "\"$cmd\" does not compute"
	}
	print STDOUT "$result\n";
}
