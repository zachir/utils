all: empty calc

empty: empty.rs
	rustc empty.rs

calc: calc.pl
	cp calc.pl calc

install: all
	@echo "Installing..."
	@echo "empty..."
	@install -Dm755 empty /usr/local/bin/empty
	@echo "calc..."
	@install -Dm755 calc /usr/local/bin/calc
	@echo "Done"

clean:
	@echo "Cleaning..."
	@rm -f *.o empty calc 2>/dev/null
	@echo "Done"
