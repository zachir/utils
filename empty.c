#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#define TRUE 1
#define FALSE 0
#define ERROR -1
int isfile (char* fname) {
	DIR* directory = opendir(fname);
	if (directory != NULL) {
		closedir(directory);
		return FALSE;
	}
	if (errno == ENOTDIR) {
		return TRUE;
	}
	return ERROR;
}
int canrw (char* fname) {
	FILE *fread = fopen(fname, "r+");
	if (fread != NULL) {
		fclose(fread);
		return TRUE;
	}
	return FALSE;
}
void errmsg(int errnum, char* errfil) {
	switch (errnum) {
		case FALSE: fprintf(stderr, "%s: is a directory!\n", errfil);
			    return;
		case ERROR: fprintf(stderr, "%s: does not exist!\n", errfil);
			    return;
		default: fprintf(stderr, "%s: something went wrong!\n", errfil);
			 return;
	}
}
int empty(char* fname) {
	FILE *fwrite = fopen(fname, "w+");
	if (fwrite != NULL) {
		fclose(fwrite);
		return TRUE;
	}
	return FALSE;
}
int main(int argc, char** argv) {
	int canopen;
	int success = FALSE;
	for (int i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			//do args
			continue;
		}
		canopen = isfile(argv[i]);
		if (canopen != TRUE) {
			errmsg(canopen, argv[i]);
			continue;
		}
		if (canrw(argv[i]))
			success = empty(argv[i]);
		if (success) {
			fprintf(stdout, "%s\n", argv[i]);
		} else {
			fprintf(stdout, "%s: did not empty!", argv[i]);
		}
	}
	return 0;
}
